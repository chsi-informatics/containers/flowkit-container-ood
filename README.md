# flowkit-container

## Overview

This repository contains the Singularity definition for building a container containing several Python libraries for flow cytometry analysis, including FlowKit, scikit-learn, jupyter, and other libraries for dimension reduction and clustering. See the flowkit-singularity.def file for a complete listing of included libraries.

This container is an adaptation of Scott White's Flowkit container meant to be compatible with Open OnDemand. 

The definition file builds from rocker/verse and contains RStudio server.

## Building

The container is built via CI/CD automatically when 'Singularity.def' is modified. 

## Pulling

The resulting sif file is only pushed to the registry when a tag is used for a commit. The latest version of the built container can be pulled from the registry using:

Pulling from FTP server (now preferred)
```
TAG="" #Define desired tag/version
wget --directory-prefix /opt/apps/community/od_chsi_rstudio --no-check-certificate "https://research-containers-01.oit.duke.edu/chsi-informatics/flowkit-container-ood_${TAG}.sif"
```

Pulling from OCI registry (now deprecated)
```
TAG="" #Define desired tag/version
apptainer pull --force --dir /opt/apps/community/od_chsi_rstudio oras://gitlab-registry.oit.duke.edu/chsi-informatics/containers/flowkit-container-ood/flowkit-container-ood:${TAG}
```

## Running

The primary method of running the container is Open OnDemand via the RStudio Singularity commmunity app. 
